import $ from 'jquery'
import { divIcons } from 'leaflet' // ? -> vu en cours mais c'est quoi ?

/**
 * GEOJSON - gros obj js ac une suite de coord gps et diff features
 * = peut représ une courbe de plusierus points gps
 * (remove geojson features :  https://gis.stackexchange.com/a/307262 )
 */
// const geoJsonGre = {"type":"FeatureCollection","features":[{"type":}]} // à compléter / trouver --> cf doc !!!

/** Récupération API lignes */
const lines = 'https://data.metromobilite.fr/api/routers/default/index/routes'

/** Récupération API horaires */
// /!\ {SEM,C38,SNC}:{X}&time={Y} à la suite de :
let times = 'http://data.metromobilite.fr/api/ficheHoraires/json?route='

// let stopTimes = 'routers/default/index/stops/{SEM,C38}:id/stoptimes/'

// Récupération DOM des ul
let $tram = $('#tram')
let $chrono = $('#chrono')
let $proximo = $('#proximo')
let $flexo = $('#flexo')

// ======================================================= AJAX LIGNES

/** Récupération API & affichage DOM des lignes */
$.ajax({
	url: lines,
	// method: "GET", // inutile car par défaut
	dataType: 'json',
})
	.done(function (res) {

		/** Récupère et paramètre les attr + affiche les listes de lignes dans leur type respectif */
		const donneesLignes = () => {

			// /** item ligne de transport déterminée par les attributs - définie dans le for */
			// !!! à choisir ici ou dans le for
			// let $ligne

			for (let i = 0; i < res.length; ++i) {

				/** Création d'une variable pour chaque attribut de ligne, en fonction de i
				 * OUUUF ! ! ! LA DESTRUCTURATION C'EST LA VIE ! °O°
				 */
				let { type, color, id, longName, shortName } = res[i]

				/** Création d'un item de liste : ligne + bg-color correspondant */
				let $ligne = $(`<li class="ligne" id="${shortName}"><a href="#">${shortName}</a></li>`).css('background-color', `#${color}`)


				// Création des lignes + affichage sur DOM
				if (type == 'TRAM') {
					$tram.append($ligne)
				} else if (type == 'CHRONO') {
					$chrono.append($ligne)
				} else if (type == 'FLEXO') {
					$flexo.append($ligne)
				} else if (type == 'PROXIMO') {
					$proximo.append($ligne)
				}
				// FIN IF //

				console.log($ligne)


				/** Au clic sur la ligne, afficher celle-ci sur la carte + popup aux arrêts avec nom de l'arrêt + horaires dans l'aside (v.2 : horaires des différentes lignes) */
				let $displayLineOnMap = $ligne.on('click', e => {
					e.preventDefault()
					// à partir de la ligne, récupérer table des arrêts avec [i].latitude et longitude, nom (horaires de passage)
					console.log('coucou')
					// ~ $($ligne.attr('href'))
					// ~ marker.bindPopup(`${name}<br>${horaires}`)

					// ======================================================= MAP

					$.ajax({
						// /** Les arrêts d'une ligne */
						// let lineStops = `${lines}/${id}/stops?`
						url: `${lines}/${id}/stops?`,
						dataType: 'json',
					})
						.done(function (res) {

							for (let u = 0 ; u <= res.length ; ++i ) {

								/** Récupère la lattitude et la longitude des points d'arrêt de la ligne */
								let { lat , lon } = res[i]
								console.log(lat, lon)

								/** Coordonnées de Grenoble - [lattitude, longitude] */
								const gre = [45.1875602, 5.7357819]

								/** Récup #map ; ([coord], zoom par déft) */
								const map = L.map('map').setView(gre, 15)

								/** Affiche un fond de carte sur la map */
								L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
									maxZoom: 20,
									attribution: '&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
								}).addTo(map)
								// L.tileLayer('https://{s}title.openstreetmap.org/{z},{x},{y}.png', // -> môrchpô


								/** TEST 'marker' prend un tableau : les coord gps du marker qu'on veut mettre ; */
								let marker = L.marker(gre).addTo(map)

								// TEST MARKER //
								// 'bindPopup' : add a popup ; 'openp' : l'ouvre au chargt ==> ! ordre important !
								let markPopup = marker.bindPopup(`GRENOBLE <br> (Préfecture)`).openPopup()
								

								let displayLineOnMap =  L.marker([lat, lon], 15).addTo(map)
								let linePopup = displayLineOnMap.bindPopup(`${longName}`).openPopup()

							}
							// FIN FOR disp lines on map
						})
						// FIN .done -> displays lines on map
						.fail(function (res, stat, erreur) {
							// afficher une erreur ?
							console.log(stat)
							console.log(erreur)
						})
					// FIN .fail -> displays lines on map
				})
				// FIN .on click -> displays lines on map
			}
			// FIN FOR display lignes //
		}
		// FIN CONST donneesLignes //
		donneesLignes()
	})
	// FIN .DONE lignes //
	.fail(function (res, stat, erreur) {
		// afficher une erreur ?
		console.log(stat)
		console.log(erreur)
	})
	// FIN .FAIL lignes //




// ==================== A FAIRE ================= //

// Au clic sur une ligne (liste) -> eventlistener click
							// Afficher la carte -> display: none -> flex ; + none le header 
							// afficher cette ligne & les arrêts sur la carte -> fetch qui récupère tous les arrêts de la ligne
							// à un nouveau clic, effacer la ligne préc de la map

							// Afficher le nom et le prochain horaire à chaque arrêt dans un aside

							// faire les deux sens -> (btn pour switcher en html) "if direction = a ; else dir = b", puis marker[i] avec nom de l'arrêt (tableau des arrêts)
							// arrêts au clic qui affichent le prochain horaire -> event listener click + récupérer la donnée de temps et tout de la ligne
							// (récupérer l'id de la ligne ? )



// ============================================= Peut servir

// // Récup sections //
// let $tramSection = $('#tramSection')
// let $chronoSection = $('#chronoSection')
// let $proximoSection = $('#proximoSection')
// let $flexoSection = $('#flexoSection')

// // création listes //
// let $tram = $('<ul id=tram></ul>') // création de la liste ul#tram
// $($tramSection).append($tram) // appel de ul#tram dans #tramSection
// let $chrono = $('<ul id=chronos></ul>')
// $($chronoSection).append($chrono)
// let $flexo = $('<ul id=flexo></ul>')
// $($flexoSection).append($flexo)
// let $proximo = $('<ul id=proximo></ul>')
// $($proximoSection).append($proximo)

// ------------------- polygon on map

// L.Polygon([
			// 	gre,
			// 	[45.171, 5.732]
			// ]).addTo(map)

			// L.geoJSON(geoJsonGre, {
			// 	style: {
			// 		color: '#777333'
			//     }
			// }).addTo(map);

// ------------------- INPUT de recherche + switch (test)

// let searchInput = $('#search')
// console.log(searchInput.get())
// searchInput.on('keydown', e => {
// 	if (e.keyCode === 13) {
// 		console.log(searchInput.value)
// 	}
// })